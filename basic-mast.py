from http.server import BaseHTTPRequestHandler, HTTPServer
from bm_classes import StreamToLogger
from io import BytesIO
import datetime
import logging
import time
import ssl
import sys
import os

hostName = "basic-mast.durish.xyz"
serverPort = 8443

wwwroot = open('public/html/index.html', 'rb').read()

class BasicMast(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(wwwroot)

if __name__ == "__main__":
    webServer = HTTPServer((hostName, serverPort), BasicMast)
    webServer.socket = ssl.wrap_socket (webServer.socket,
        keyfile=os.getenv('SSL_PRIVATE_KEY'),
        certfile=os.getenv('SSL_CERTIFICATE_FILE'), server_side=True)
    print("Server started http://%s:%s" % (hostName, serverPort))

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print("Server stopped.")
